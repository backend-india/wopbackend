package com.wop.coutchdb.configuration;

import java.net.MalformedURLException;

import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("couchdb")
public class CouchDBConfiguration {

	private String dbName;
	
	private String url;


	public void setDbName(String dbName) {
		this.dbName = dbName;
	}


	
	public void setUrl(String url) {
		this.url = url;
	}

	@Bean
	CouchDbConnector dbConnector() throws MalformedURLException {
		HttpClient authenticatedHttpClient = new StdHttpClient.Builder().url(url)
				// .username("admin")
				// .password("secret")
				.build();
		CouchDbInstance dbInstance = new StdCouchDbInstance(authenticatedHttpClient);
		CouchDbConnector db = dbInstance.createConnector(dbName, true);
		
		return db;

	}

}
