package com.wop.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wop.entity.WopUser;
import com.wop.repositories.WOPUserRepository;

@RestController
@RequestMapping(value = "/signin")
public class LoginController {

	@Value("${facebook.redirectURL}")
	String redirectURL;
	
	@Autowired
	WOPUserRepository wOPUserRepository;
	
	@Autowired
	private FacebookConnectionFactory facebookConnectionFactory;
	

	@RequestMapping(value = "/login/facebook" , method = RequestMethod.GET)
	public void facebookSignIn(HttpServletRequest request,
			HttpServletResponse response) {

		facebookConnectionFactory.setScope("public_profile,email");
		OAuth2Operations oauthOperations = facebookConnectionFactory
				.getOAuthOperations();
		OAuth2Parameters params = new OAuth2Parameters();
		String host = request.getServerName();
		String scheme = request.getScheme();
		int port = request.getServerPort();
		params.setRedirectUri(scheme + "://" + host + ":" + port
				+ redirectURL);
		String authorizeUrl = oauthOperations.buildAuthorizeUrl(
				GrantType.AUTHORIZATION_CODE, params);
		try {
			response.sendRedirect(authorizeUrl);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/login/facebook/callback" , method = RequestMethod.GET)
	public WopUser facebookSignInCallback(@RequestParam String code,
			HttpServletRequest request, HttpServletResponse response) throws IOException {

		OAuth2Operations oauthOperations = facebookConnectionFactory
				.getOAuthOperations();
		String authorizationCode = code;
		String host = request.getServerName();
		String scheme = request.getScheme();
		int port = request.getServerPort();
		AccessGrant accessGrant = oauthOperations.exchangeForAccess(
				authorizationCode, scheme + "://" + host + ":" + port
						+ redirectURL, null);
		Connection<Facebook> connection = facebookConnectionFactory
				.createConnection(accessGrant);
		Facebook facebook = connection.getApi();
		Object profile = facebook.userOperations().getUserProfile();
		
		WopUser wopUser = populateUser(profile);
		
		if(wOPUserRepository.get(wopUser.getId())==null){
			wOPUserRepository.add(wopUser);
		}
		
		return wopUser;

	}

	private WopUser populateUser(Object facebookObject){
		WopUser wopUser = new WopUser();
		if(facebookObject instanceof User){
			org.springframework.social.facebook.api.User user =(org.springframework.social.facebook.api.User)facebookObject;
			wopUser.setId(user.getId());
			wopUser.setEmailId(user.getEmail());
			wopUser.setFirstName(user.getFirstName());
			wopUser.setLastName(user.getLastName());
			wopUser.setSex(user.getGender());
		}
		
		return wopUser;
		
	}
}
