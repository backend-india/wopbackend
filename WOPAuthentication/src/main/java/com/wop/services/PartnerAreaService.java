package com.wop.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.wop.entity.PartnerArea;
import com.wop.repositories.WOPPartnerAreaRepository;

@Scope(value = "singleton")
public class PartnerAreaService {
	
	@Autowired
	WOPPartnerAreaRepository partnerAreaRepository;

	public List<PartnerArea> getAll() {
		return partnerAreaRepository.getAll();
	}

	public void add(PartnerArea partnerArea) {
		partnerAreaRepository.add(partnerArea);
	}

	public List<PartnerArea> findByPartnerAreaId(String partnerAreaId) {
		return partnerAreaRepository.findByPartnerAreaId(partnerAreaId);
	}

}
