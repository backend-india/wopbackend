package com.wop.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.wop.entity.Ticket;
import com.wop.repositories.WOPTicketingRepository;

@Scope(value = "singleton")
public class TicketingService {
	
	@Autowired
	WOPTicketingRepository ticketingRepository;

	public List<Ticket> getAll() {
		return ticketingRepository.getAll();
	}

	public void purchaseTicket(List<Ticket> tickets) {
		ticketingRepository.purchase(tickets);
	}

	public Ticket retrieveTicketById(String id) {
		return ticketingRepository.retrieveTicketById(id);
	}

}
