package com.wop;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.wop.entity.PartnerArea;
import com.wop.entity.Ticket;
import com.wop.services.PartnerAreaService;
import com.wop.services.TicketingService;

/**
 * This class is implementing CommandLineRunner for test purpose. Calls within
 * run methods are also for testing.
 * 
 * @author mujain
 *
 */
@SpringBootApplication
public class WopAuthenticationApplication implements CommandLineRunner {

	@Autowired
	PartnerAreaService partnerAreaService;
	
	@Autowired
	TicketingService ticketingService;

	public static void main(String[] args) {
		SpringApplication.run(WopAuthenticationApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {

		// run method is written for test purpose: To test purchase service
		PartnerArea partnerArea = new PartnerArea();
		// Add partnerArea
		partnerAreaService.add(partnerArea);

		partnerAreaService.findByPartnerAreaId(partnerArea.getPartnerAreaId());

		// Find Offer user wants to purchase, add make List<Ticket>
		
		List<Ticket> tickets = new ArrayList<Ticket>();
		
		ticketingService.purchaseTicket(tickets);
		
		

	}
}
