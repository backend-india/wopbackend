package com.wop.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;

@Configuration
public class SocialConfiguration {
	
	@Value("${facebook.appId}")
	String appId;
	
	@Value("${facebook.appSecret}")
	String appSecret;
	
	

	@Bean
	public FacebookConnectionFactory getFacebookConnectionFactory() {
		return new FacebookConnectionFactory(appId,appSecret);
	}

}
