package com.wop.entity;

import java.util.Date;

import org.ektorp.support.CouchDbDocument;

public class Purchase extends CouchDbDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String userId;
	String paymentMethod;
	Double paymentAmount;
	Date purchaseDate;

	// I think, we here need List of an Object which will contain offerId, ticketId
	// partnerAreaId (needed offer
	// details) along with quantity purchased for each offer. This need to be stored
	// in DB while making purchase.
}
