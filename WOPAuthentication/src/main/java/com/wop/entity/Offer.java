package com.wop.entity;

import java.util.List;

import org.ektorp.support.CouchDbDocument;

public class Offer extends CouchDbDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// final String type = "offer";
	String offerId;
	// String partnerAreaId;
	String ticketTypeId;
	int ticketQuantity;
	Price price;
	List<String> offerConditions;
	String offerDescription;
	String offerDescription2;

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public String getTicketTypeId() {
		return ticketTypeId;
	}

	public void setTicketTypeId(String ticketTypeId) {
		this.ticketTypeId = ticketTypeId;
	}

	public int getTicketQuantity() {
		return ticketQuantity;
	}

	public void setTicketQuantity(int ticketQuantity) {
		this.ticketQuantity = ticketQuantity;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public List<String> getOfferConditions() {
		return offerConditions;
	}

	public void setOfferConditions(List<String> offerConditions) {
		this.offerConditions = offerConditions;
	}

	public String getOfferDescription() {
		return offerDescription;
	}

	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}

	public String getOfferDescription2() {
		return offerDescription2;
	}

	public void setOfferDescription2(String offerDescription2) {
		this.offerDescription2 = offerDescription2;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
