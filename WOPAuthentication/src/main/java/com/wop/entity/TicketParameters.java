package com.wop.entity;

import java.util.List;

import org.ektorp.support.CouchDbDocument;

public class TicketParameters extends CouchDbDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// final String type = "parameters";
	String ticketTypeId;
	List<String> ticketConditions;
	String ticketDescription;
	String ticketDescription2;
	Double ticketValidityDuration;

	public String getTicketTypeId() {
		return ticketTypeId;
	}

	public void setTicketTypeId(String ticketTypeId) {
		this.ticketTypeId = ticketTypeId;
	}

	public List<String> getTicketConditions() {
		return ticketConditions;
	}

	public void setTicketConditions(List<String> ticketConditions) {
		this.ticketConditions = ticketConditions;
	}

	public String getTicketDescription() {
		return ticketDescription;
	}

	public void setTicketDescription(String ticketDescription) {
		this.ticketDescription = ticketDescription;
	}

	public String getTicketDescription2() {
		return ticketDescription2;
	}

	public void setTicketDescription2(String ticketDescription2) {
		this.ticketDescription2 = ticketDescription2;
	}

	public Double getTicketValidityDuration() {
		return ticketValidityDuration;
	}

	public void setTicketValidityDuration(Double ticketValidityDuration) {
		this.ticketValidityDuration = ticketValidityDuration;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
