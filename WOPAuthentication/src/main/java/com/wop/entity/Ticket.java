package com.wop.entity;

import java.util.Date;
import java.util.List;

import org.ektorp.support.CouchDbDocument;

public class Ticket extends CouchDbDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String userId;
	String ticketTypeId;
	String partnerId;
	String areaId;
	Date purchaseDate;
	Date validationTimestamp;
	Double ticketValidityDuration;
	Date validToTimestamp;
	List<String> ticketConditions;
	String ticketDescription;
	String ticketDescription2;

	// I think, we also need ticket costing details here. 
	// And it need to be actual price of the ticket bought in offer

}
