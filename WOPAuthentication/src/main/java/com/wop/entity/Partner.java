package com.wop.entity;

import java.util.Set;

import org.ektorp.docref.DocumentReferences;
import org.ektorp.docref.FetchType;
import org.ektorp.support.CouchDbDocument;

public class Partner extends CouchDbDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String partnerId;
	String partnerLogoURL;

	@DocumentReferences(backReference = "partnerId", fetch = FetchType.LAZY, descendingSortOrder = true, orderBy = "partnerAreaName")
	private Set<PartnerArea> partnerArea;
	// In case above, idea of backReference is to refer id of Partner class from
	// backReferenced field
	// i.e. partnerId from PartnerArea class. Check if id field (in extended
	// CouchDbDocument) shall contain partnerId value

}
