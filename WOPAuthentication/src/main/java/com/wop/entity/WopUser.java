package com.wop.entity;

import org.ektorp.support.CouchDbDocument;

public class WopUser extends CouchDbDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1077011935258412371L;

	String emailId;
	String firstName;
	String lastName;
	String sex;
	String phone;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
