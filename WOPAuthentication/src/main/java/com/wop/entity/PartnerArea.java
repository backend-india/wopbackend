package com.wop.entity;

import java.util.Set;

import org.ektorp.support.CouchDbDocument;

public class PartnerArea extends CouchDbDocument {

	/**
	 * On View Collation and Linked Documents
	 * https://stackoverflow.com/questions/1530745/principles-for-modeling-couchdb-documents
	 * On view Collation
	 * https://web.archive.org/web/20080222001534/http://www.cmlenz.net/archives/2007/10/couchdb-joins
	 * On Linked Documents
	 * https://wiki.apache.org/couchdb/Introduction_to_CouchDB_views#Linked_documents
	 * 
	 * Idea is to create view and emit key/values for
	 * partnerAreaId-offerId-ticketTypeId(ticketParameters). So that, system
	 * retrieves entire offer details of partner area when queried.
	 * 
	 * Or Alternatively, we go with @DocumentReferences annotation as coded below
	 * for offer. Now the question is, how to fetch Ticket Parameters for given
	 * ticketTypeId of the Offer.
	 * 
	 * Ektorp Reference https://helun.github.io/Ektorp/reference_documentation.html
	 * Ektorp Tutorial: https://helun.github.io/Ektorp/tutorial.html
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// final String type = "partnerArea";
	String partnerAreaId;
	String areaId;
	String partnerId;
	String mapURL;
	Set<Offer> offers;
	Set<TicketParameters> ticketParams;

	public String getPartnerAreaId() {
		return partnerAreaId;
	}

	public void setPartnerAreaId(String partnerAreaId) {
		this.partnerAreaId = partnerAreaId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getMapURL() {
		return mapURL;
	}

	public void setMapURL(String mapURL) {
		this.mapURL = mapURL;
	}

	public Set<Offer> getOffers() {
		return offers;
	}

	public void setOffers(Set<Offer> offers) {
		this.offers = offers;
	}

	public Set<TicketParameters> getTicketParams() {
		return ticketParams;
	}

	public void setTicketParams(Set<TicketParameters> ticketParams) {
		this.ticketParams = ticketParams;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
