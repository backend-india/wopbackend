package com.wop.repositories;

import java.util.List;

import org.ektorp.CouchDbConnector;
import org.ektorp.support.CouchDbRepositorySupport;
import org.ektorp.support.GenerateView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.wop.entity.WopUser;

@Scope(value = "singleton")
@Repository
public class WOPUserRepository extends CouchDbRepositorySupport<WopUser> {

	@Autowired
	public WOPUserRepository(CouchDbConnector db) {
		super(WopUser.class, db);
		initStandardDesignDocument();
	}

	@Override
	public List<WopUser> getAll() {
		return super.getAll();
	}

	@Override
	public void add(WopUser user) {
		super.add(user);
	}

	@GenerateView
	public List<WopUser> findByEmailId(String emailId) {
		List<WopUser> userList = queryView("by_emailId", emailId);
		return userList;
	}
}