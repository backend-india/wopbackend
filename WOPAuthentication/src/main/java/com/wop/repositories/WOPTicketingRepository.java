package com.wop.repositories;

import java.util.List;

import org.ektorp.CouchDbConnector;
import org.ektorp.support.CouchDbRepositorySupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.wop.entity.Ticket;

@Scope(value = "singleton")
@Repository
public class WOPTicketingRepository extends CouchDbRepositorySupport<Ticket> {

	@Autowired
	public WOPTicketingRepository(CouchDbConnector db) {
		super(Ticket.class, db);
		initStandardDesignDocument();
	}

	@Override
	public List<Ticket> getAll() {
		return super.getAll();
	}


	public void purchase(List<Ticket> tickets) {
		for(Ticket ticket : tickets) {
			super.add(ticket);	
		}
		
	}

	public Ticket retrieveTicketById(String id) {
		return super.get(id);
	}

}