package com.wop.repositories;

import java.util.List;

import org.ektorp.CouchDbConnector;
import org.ektorp.support.CouchDbRepositorySupport;
import org.ektorp.support.GenerateView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.wop.entity.PartnerArea;

@Scope(value = "singleton")
@Repository
public class WOPPartnerAreaRepository extends CouchDbRepositorySupport<PartnerArea> {

	@Autowired
	public WOPPartnerAreaRepository(CouchDbConnector db) {
		super(PartnerArea.class, db);
		initStandardDesignDocument();
	}

	@Override
	public List<PartnerArea> getAll() {
		return super.getAll();
	}

	@Override
	public void add(PartnerArea partnerArea) {
		super.add(partnerArea);
	}

	@GenerateView
	public List<PartnerArea> findByPartnerAreaId(String partnerAreaId) {
		List<PartnerArea> partnerAreaList = queryView("by_partnerAreaId", partnerAreaId);
		return partnerAreaList;
	}

}